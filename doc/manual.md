# Manual de uso - EP1 Orientação a Objetos

**Aluno**: Josué Bezerra Bonfim Filho  
**Matrícula**: 16/0032598  

O programa é iniciado com o Menu Principal, apresentando as seguintes opções:

`Menu Principal` <br/>
`-----------------------` <br/>
`1 - Modo Venda` <br/>
`2 - Modo Estoque` <br/>
`3 - Modo Recomendação` <br/>
`0 - Sair` <br/>
`-----------------------` <br/>
`Digite a opção desejada:` <br/>

De acordo com o a instrução do menu, pode-se selecionar as opções e acessar os modos, os quais são: `Modo Venda`, `Modo Estoque` e `Modo Recomendação(não implementado)`.

## Modo Venda

Ao acessar o `Modo Venda`, o usuário é direcionado ao menu, apresentado abaixo.

`-----------------------------------------` <br/>
`Modo Venda ` <br/>
`-----------------------------------------` <br/>
`Digite o nome do Cliente: `

Uma vez que o usuário digita o nome do cliente, é realizada uma busca nos arquivos. Se o arquivo não existir, a mensagem `Não foi possível abrir o arquivo.` é mostrada e um novo arquivo é criado. Se o cliente não estiver cadastrado, o usuário é direcionado para a sessão de cadastro de clientes. 

### Cadastro do Cliente

Para o cadastro, é pedido que o usuário entre com o nome, CPF, e-mail, telefone e data de nascimento para cadastro, de acordo com o as opções abaixo:

`-----------------------------` <br/>
`Digite o nome do Cliente: ` <br/>
`Digite o CPF do Cliente: ` <br/>
`Digite o e-mail do Cliente: ` <br/>
`Digite o telefone do Cliente: ` <br/>
`Digite a data de nascimento do Cliente: ` <br/>

Com estes dados, o cliente é salvo em um arquivo binário.

### Venda dos Produtos

Após a verificação e possível cadastro do cliente, o usuário é instruído a colocar o nome do produto e quantidade do produto. Se a quantidade é maior que a existente em estoque, a compra é cancelada e o programa encerrado por uma exceção. Se existir, a compra continua e é perguntado ao usuário se ele deseja adicionar mais produtos, de acordo com o menu abaixo:

`Deseja adicionar mais um produto? ` <br/>
`0 - Não` <br/>
`1 - Sim` <br/>
`Digite a opção: ` <br/>

Se o usuário não quiser adicionar mais produtos a compra é encerrada, mostrando o total das compras. Se o cliente for sócio, ele recebe 15% de desconto.

## Modo Estoque

Ao entrar no modo estoque, é apresentado ao usuário um menu de opções, como é apresentado abaixo:

`Modo Estoque` <br/>
`---------------------` <br/>
`1 - Cadastrar Novo Produto` <br/>
`2 - Atualizar Produto` <br/>
`3 - Excluir Produto` <br/>
`4 - Mostrar Lista de Produtos Cadastrados` <br/>
`0 - Voltar ao Menu Principal` <br/>

### Cadastrar Novo Produto

O usuário deve digitar o nome do produto, sua quantidade em estoque, seu preço e categoria.

### Atualiza Produto

O usuário deve digitar o nome do produto. Se o produto existir, ele deve selecionar o que ele deseja alterar, como mostra o menu abaixo:

`1. Alterar Quantidade` <br/>
`2. Alterar Preço` <br/>
`3. Alterar Categoria` <br/>
`Digite a opção desejada:` <br/>

Selecionando a opção desejada, o usuário altera o que ele precisa e o produto é salvo no arquivo binário.

Se o produto não existir, o programa volta ao menu do `Modo Estoque`.

### Exclui Produto

O usuário deve digitar o nome do produto, e então o programa irá excluir o produto.

### Mostrar Lista de Produtos Cadastrados

O programa mostra ao usuário todos os produtos cadastrados.

## Modo Recomendação

Não foi implementado
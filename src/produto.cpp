#include <iostream>
#include "produto.hpp"
#include "funcoes_uteis.hpp"
#include "excecao.hpp"

using namespace std;

Produto::Produto(){}

Produto::Produto(string nome, int quantidade, string categoria){
    set_nome(nome);
    set_tipo("Produto");
    set_quantidade(quantidade);
    set_categoria(categoria);
}

Produto::~Produto(){}

int Produto::get_quantidade()
{
    return quantidade;
}

void Produto::set_quantidade(int quantidade)
{
    if(quantidade < 0)
        throw Excecao("Quantidade deve ser maior que zero!");
    this->quantidade = quantidade;
}

float Produto::get_preco()
{
    return preco;
}

void Produto::set_preco(float preco)
{
    if(preco < 0)
        throw Excecao("Preco deve ser maior que zero! ");
    this->preco = preco;
}

string Produto::get_categoria()
{
    return categoria;
}

void Produto::set_categoria(string categoria)
{
    this->categoria = categoria;
}

void Produto::imprime_dados()
{
    cout << "--------------------------" << endl;
    cout << "Nome: " << get_nome() << endl;
    cout << "Preco: " << get_preco() << endl;
    cout << "Quantidade em estoque: " << get_quantidade() << endl;
    cout << "Categoria: " << get_categoria() << endl;
    cout << "-------------------------" << endl << endl;
}

void Produto::cadastra_produto()
{
    string nome_temp;
    int qtd_temp;
    float preco_temp;
    string categoria_temp;

    cout << endl;
    cout << "Cadastro de Novo Produto" << endl;
    cout << "------------------------" << endl;
    cout << "Digite o nome do produto: ";
    set_nome(get_string());
    cout << "Digite a quantidade em estoque: ";
    qtd_temp = get_int();
    set_quantidade(qtd_temp);
    cout << "Digite o preço do produto: ";
    preco_temp = get_float();
    set_preco(preco_temp);
    cout << "Digite a categoria do produto: ";
    set_categoria(get_string());
}


#include <iostream>
#include "cliente.hpp"
#include "produto.hpp"
#include "funcoes_uteis.hpp"
#include "excecao.hpp"
#include <vector>
#include <fstream>
#include <string>

using namespace std;

void mostraMenuPrincipal();
void modoVenda();
void modoEstoque();
void mostraMenuEstoque();
void cadastraProduto();
void atualizaProduto();
void excluiProduto();
void mostraProdutos();
void verifica_quantidade(int qtd, Produto objeto);



int main(int argc, char *argv[])
{
    int opcao;
    while(1)
    {
        mostraMenuPrincipal();
        opcao = get_int();
        system("clear");
        if(opcao == 0)
            break;
        switch (opcao)
        {
        case 1:
            //cout << endl << endl << endl << endl;
            modoVenda();
            /* cout << "Modo em manutenção" << endl;
            system("sleep 2"); */
            break;
        case 2:
            //modoEstoque();
            /* cout << "Modo em manutenção" << endl;
            system("sleep 2"); */
            modoEstoque();
            break;
        case 3:
            //modoRecomendacao();
            cout << "Modo em manutenção" << endl;
            system("sleep 2");
            break;
        default:
            break;
        }
    }
    return 0;
}

void mostraMenuPrincipal(){
    cout << "Menu Principal" << endl;
    cout << "-----------------------" << endl << endl;
    cout << "1 - Modo Venda" << endl;
    cout << "2 - Modo Estoque" << endl;
    cout << "3 - Modo Recomendação" << endl;
    cout << "0 - Sair" << endl;
    cout << "-----------------------" << endl;
    cout << "Digite a opção desejada: ";
}

void modoVenda()
{
    Cliente cliente, temp_cliente;
    fstream file;
    Produto *produto, temp_produto, *carrinho;
    int qtd, qtd_produto, count = 0, conta_cliente_cadastrado = 0,conta_produto_existente = 0, continua = 0, j = 0, qtd_carrinho = 0;
    string nome_temp, produto_temp, temp_string;
    float total_sem_desconto = 0.0f, total_com_desconto = 0.0f;

    cout << "-----------------------------------------" << endl;
    cout << "Modo Venda " << endl;
    cout << "-----------------------------------------" << endl;

    cout << "Digite o nome do Cliente: ";
    nome_temp = get_string();
    file.open("cliente.bin", ios::in | ios::binary);
    if(file.fail())
    {
        cout<< "Nao foi possivel abrir o arquivo." << endl; 
        file.close();
        file.open("cliente.bin", ios::out | ios::app | ios::binary);
        file.close();
    }
    while(!file.eof())
    {
        file.read((char*)&temp_cliente, sizeof(Cliente));
        //temp_cliente.imprime_dados();
        count++;
    }
    count--;
    file.clear();
    file.seekg(0);

    for(int i = 0; i < count; i++)
    {
        file.read((char*)&temp_cliente, sizeof(Cliente));
        if(!(nome_temp.compare(temp_cliente.get_nome())))
        {
            conta_cliente_cadastrado++;
        }
        else
            cliente = temp_cliente;
        
    }
    file.close();
    if(conta_cliente_cadastrado == 0)
    {
        cliente.cadastra_cliente();
        file.open("cliente.bin", ios::in | ios::app | ios::binary);
        file.write((char*)&cliente, sizeof(Cliente));
        file.close();
        cout << "Bem-vindo " << cliente.get_nome() << "!" << endl << endl;
    }
    else{
        cout << "Bem-vindo " <<  nome_temp << "!" << endl << endl;
    }
    file.open("produto.bin", ios::in | ios::binary);
    while(!file.eof())
    {
        file.read((char*)&temp_produto, sizeof(Produto));
        j++;
    }
    j--;
    qtd_produto = j;
    file.clear();
    file.seekg(0);
    produto = (Produto*)malloc(qtd_produto*sizeof(Produto));
    for(int i = 0; i < j; i++)
    {
        file.read((char*)&temp_produto, sizeof(Produto));
        *(produto+i) = temp_produto;
    }
    file.close();

    carrinho = (Produto *) malloc(sizeof(Produto));
    do
    {
        
        cout << "Digite o nome do produto: ";
        produto_temp = get_string();
        j = 0;
        conta_produto_existente = 0;

        while(j < qtd_produto)
        {
            if(produto_temp == (produto+j)->get_nome())
            {
                conta_produto_existente++;
                count = j;
                break;
            }
            j++;
        }
        if(conta_produto_existente > 0)
        {
            try
            {
                cout << "Digite a quantidade: ";
                qtd = get_int();
            /* if(qtd > (produto+count)->get_quantidade())
            {
                cout << "Quantidade não existe em estoque. A compra será cancelada." << endl;
                break;

            } */
                verifica_quantidade(qtd, *(produto+count));
            }catch(Excecao & e)
            {
                cout << e.what() << endl;
            }
            total_sem_desconto += qtd*(produto+count)->get_preco();
            (produto + count)->set_quantidade((produto+count)->get_quantidade() - qtd);
            cout << "Total parcial das compras: " << total_sem_desconto << endl;
            *(carrinho + qtd_carrinho) = *(produto+count);
            (carrinho + qtd_carrinho)->set_quantidade(qtd);
            qtd_carrinho++;
        }else
        {
            cout << "Produto Inexixtente";
            break;
        }
        if(qtd_produto)
        {
            cout << "Deseja adicionar mais um produto?" << endl; 
            cout << "0 - Não" << endl << "1 - Sim" << endl;
            cout << "Digite a opcao: ";
            continua = get_int();
        }
        else
            break;    
    }while(continua);
    system("clear");
    system("rm produto.bin");
    file.open("produto.bin", ios::out | ios::app | ios::binary);
    for(int i = 0; i < qtd_produto; i++)
    {
        file.write((char*)(produto+i), sizeof(Produto));
    }
    file.close();
    free(produto);  

    if(qtd_carrinho)
    {
        cout << "Lista de Produtos Comprados: " << endl;
        cout << "------------------------------" << endl;
        for(int i = 0; i < qtd_carrinho; i++)
        {
            cout << "Produto: " << (carrinho + i)->get_nome() << endl;
            cout << "Quantidade: " << (carrinho + i)->get_quantidade() << endl;
            cout << "Preço: " << (carrinho + i)-> get_preco() << endl;
            cout << "------------------------------" << endl;
        }
    
        cout << endl;
        cout << "Total das compras: " << total_sem_desconto << endl;
        cliente.set_qtd_de_compras(cliente.get_qtd_de_compras() + 1);
        if(cliente.get_socio())
        {
            total_com_desconto = total_sem_desconto * 0.85;
            cout << "Total a pagar: " << total_com_desconto << endl;
        }else
            cout << "Total a pagar: " << total_sem_desconto << endl;
        system("sleep 2");
        
    }
    free(carrinho);
}


void modoEstoque(){
    int opcao;
    while(1)
    {    
        mostraMenuEstoque();
        cout << "Digite a opção desejada: ";
        opcao = get_int();
        system("clear");
        if(opcao == 0)
            break;
        switch (opcao)
        {
        case 1:
            //cadastrar o produto
            //cout << "Modo em manutenção" << endl;
            //system("sleep 2");
            cadastraProduto();
            break;
        case 2:
            cout << "Modo em manutenção" << endl;
            atualizaProduto();
            break;
        case 3:
            cout << "Modo em manutenção" << endl;
            system("sleep 2");
            excluiProduto();
            break;
        case 4:
            /* cout << "Modo em manutenção" << endl;
            system("sleep 2"); */
            mostraProdutos();
            //mostra todos os produtos
            break;
            //mostraProdutos();
        default:
            break;
        }

    }
}

void mostraMenuEstoque(){
    cout << "Modo Estoque" << endl;
    cout << "---------------------" << endl;
    cout << "1 - Cadastrar Novo Produto" << endl;
    cout << "2 - Atualizar Produto" << endl;
    cout << "3 - Excluir Produto" << endl;
    cout << "4 - Mostrar Lista de Produtos Cadastrados" << endl;
    cout << "0 - Voltar ao Menu Principal" << endl;
}

void cadastraProduto()
{
    fstream file;
    Produto temp;
    temp.cadastra_produto();
    file.open("produto.bin", ios::in | ios::app | ios::binary);
    file.write((char*)&temp, sizeof(Produto));
    if(!file.bad())
        cout << "Produto Cadastrado com Sucesso!" << endl;
    else 
        cout << "Falha ao salvar o arquivo" << endl;
    file.close();
    system("clear");
}

void atualizaProduto()
{
    string temp_nome;
    Produto produto, temp_produto, temp2;
    fstream file, temp_file;
    int count = 0, ja_existe = 0, opcao;

    int quantidade;
    float preco;
    string categoria;

    cout << "Digite o nome do produto: ";
    temp_nome = get_string();
    file.open("produto.bin", ios::in | ios::binary);
    while(!file.eof())
    {
        file.read((char*)&produto, sizeof(produto));
        count++;
    }
    count--;
    file.clear();
    file.seekg(0);
    temp_file.open("temp.bin", ios::out | ios::binary);
    for(int i = 0; i < count; i++)
    {
        file.read((char*)&produto, sizeof(produto));
        if(temp_nome != produto.get_nome())
        {
            temp_produto = produto;
            temp_file.write((char*)&temp_produto, sizeof(temp_produto));
            ja_existe++;
        }  
        else
            temp2 = produto; ja_existe = 0;
    }
    if(ja_existe == 0)
    {
        cout << "1. Alterar Quantidade" << endl;
        cout << "2. Alterar Preço" << endl;
        cout << "3. Alterar Categoria" << endl;
        cout << "Digite a opção desejada: ";
        opcao = get_int();
        switch(opcao)
        {
            case 1:
                cout << "Digite a nova quantidade: ";
                quantidade = get_int();
                temp2.set_quantidade(quantidade);
                temp2.imprime_dados();
                break;
            case 2:
                cout << "Digite o novo preço: ";
                preco = get_float();
                temp2.set_preco(preco);
                break;
            case 3:
                cout << "Digite a nova categoria: ";
                categoria = get_string();
                temp2.set_categoria(categoria);
                break;
            default:
                break;
        }
    }
    temp_file.write((char*)&temp2, sizeof(temp2));
    file.close();
    temp_file.close();
    system("rm produto.bin");
    system("mv temp.bin produto.bin");
}

void excluiProduto()
{
    /* cout << "Em manutenção" << endl;
    system("sleep 2"); */
    string temp_nome;
    Produto produto, temp_produto;
    fstream file, temp_file;
    int count = 0, ja_existe = 0;

    cout << "Digite o nome do produto: " << endl;
    temp_nome = get_string();
    file.open("produto.bin", ios::in | ios::binary);
    while(!file.eof())
    {
        file.read((char*)&produto, sizeof(produto));
        count++;
    }
    count--;
    file.clear();
    file.seekg(0);
    temp_file.open("temp.bin", ios::out | ios::binary);
    for(int i = 0; i < count; i++)
    {
        file.read((char*)&produto, sizeof(produto));
        if(temp_nome != produto.get_nome())
        {
            cout << "entrou";
            temp_produto = produto;
            temp_file.write((char*)&temp_produto, sizeof(temp_produto));
            ja_existe++;
        }
    }
    file.close();
    temp_file.close();
    if(!ja_existe)
    {
        system("rm produto.bin");
        system("mv temp.bin produto.bin");
        cout << "Produto não está cadastrado!";
    }
    else
    {    
        system("rm produto.bin");
        system("mv temp.bin produto.bin");
    }
}

void mostraProdutos()
{
    /* cout << "Em manutenção" << endl;
    system("sleep 2"); */
    fstream f_produtos;
    Produto produto;
    int j = 0;

    f_produtos.open("produto.bin", ios::in | ios::binary);
    while(!f_produtos.eof())
    {
        f_produtos.read((char*)&produto, sizeof(Produto));
        j++;
    }
    j--;
    f_produtos.clear();
    f_produtos.seekg(0);
    for(int i = 0; i < j; i++)
    {
        f_produtos.read((char*)&produto, sizeof(Produto));
        produto.imprime_dados();
    }
    f_produtos.close();
}

void verifica_quantidade(int qtd, Produto objeto)
{
    if(qtd > objeto.get_quantidade())
        throw Excecao("Quantidade não existe em estoque. A compra será cancelada");
}
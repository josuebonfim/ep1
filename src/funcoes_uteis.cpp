#include "funcoes_uteis.hpp"


string get_string()
{
    //ainda tá salvando alguma coisa do buffer
    string valor;
    getline(cin, valor);
    cin.clear();
    cin.ignore(32767, '\n');
    valor = valida_string(valor);
    return valor;
}

string valida_string(string valor)
{
    while(valor.length() < 1)
    {
        cout << "A entrada não pode ser vazia!" << endl;
        cout << "Digite novamente: ";
        getline(cin, valor);
        cin.clear();
        cin.ignore(32767, '\n');
    }
    return valor;
}

float get_float()
{
    float valor;
    cin >> valor;
    while(valor < 0)
    {
        cin.clear();
        cin.ignore();
        cout << "Entrada Inválida!" << endl;
        cout << "Digite novamente: ";
        cin >> valor;
    }
    cin.clear();
    cin.ignore();
    return valor;
}

int get_int()
{
    int valor;
    cin >> valor;
    while(valor < 0)
    {
        cin.clear();
        cin.ignore();
        cout << "Entrada Inválida!" << endl;
        cout << "Digite novamente: ";
        cin >> valor;
    }
    cin.clear();
    cin.ignore();
    return valor;
}

long int get_long_int()
{
    long int valor;
    cin >> valor;
    while(valor < 0)
    {
        cin.clear();
        cin.ignore();
        cout << "Entrada Inválida!" << endl;
        cout << "Digite novamente: ";
        cin >> valor;
    }
    cin.clear();
    cin.ignore();
    return valor;
}


/* template <typename T1> T1 get_input()
{
    T1 valor;
    cin >> valor;
    while(valor < 0)
    {
        cin.clear();
        cin.ignore();
        cout << "Entrada Inválida!" << endl;
        cout << "Digite novamente: ";
        cin >> valor;
    }
    cin.clear();
    cin.ignore();
    return valor;
} */
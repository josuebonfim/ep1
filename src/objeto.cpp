#include <iostream>
#include "objeto.hpp"

using namespace std;

Objeto::Objeto(){}

Objeto::Objeto(string nome, string tipo){
    set_nome(nome);
    set_tipo(tipo);
}

Objeto::~Objeto(){}

string Objeto::get_nome(){
    return nome;
}

void Objeto::set_nome(string nome){
    this->nome = nome;
}

string Objeto::get_tipo(){
    return tipo;
}

void Objeto::set_tipo(string tipo){
    this->tipo = tipo;
}

# Esboço do Desenho Geral do EP1

Esboço geral da forma final do EP1 da Disciplina Orientação a Objetos

## Modo Venda

Prazo para o término da implementação - 26/09

- Nova Venda 
  - Digitar o nome do Cliente
    - Se ele não existir, realizar o cadastro e então continua a venda
    - Se existir, continuar com a venda
    - colocar os produtos
    - se um produto não estiver no estoque, lançar uma exceção e parar a execução do produto
    - se o cliente for sócio, dar o desconto de 15% e mostrar o desconto
    - senão, fechar a compra
  - Cadastro de Usuários

## Modo Estoque 

Prazo para o término da implementação - 26/09

- Verificar o estoque
  - Ler o arquivo `produto.bin` e mostrar na tela
- Cadastro de Produtos
  - Ler  da entrada os produtos e salvar no `produto.bin`.
- Cadastro de Categoria
  - Muito obscuro ainda


## Modo Recomendação

Prazo para o término da implementação - 29/09

- Muito obscuro ainda (25/09)


## Tarefas - 24/09

Previsão 16 a 18 pomodoros

- [x] Adicionar ao cliente a quantidade de vendas
  - [x] Após o cliente ter realizado um número de compras, ele se torna sócio
- [x] Implementar a sociedade
- [x] Implementar o cadastro do Cliente como um método
  - [x] Salvar o novo cliente no arquivo
  - [x] Ler o novo Cliente em arquivo
- [x] Começar a implementação do Modo Estoque
  - [x] Cadastro dos produtos
  - [x] Salvar em arquivo
  - [x] Leitura dos Produtos salvos no arquivo

## Tarefas - 25/09

- [x] Fazer o desenho dos menus
- [x] Começar a implementação da Main
  - [x] Fazer o menu principal
  - [x] Fazer o menu do modo venda
  - [x] Fazer o menu do modo recomendação

## Tarefas - 26/09

- [x] Implementação total do modo estoque
  - [x] Implementação do Cadastro
  - [x] Implementação da Alteração
  - [x] Implementação da Exclusão
  - [x] Implementação da Mostra de todos os produtos

## Tarefas - 27/09 e 28/09

- [ ] Implementar o Modo Venda

## Tarefas - 29/09 

- [x] Implementar o Modo Venda
- [x] Fazer as validações no Produto
  - [x] Quantidade sempre positiva maior que zero
  - [x] Preço sempre positivo maior que zero
- [ ] Fazer as validações no Cliente
  - [ ] CPF único ?
  - [ ] Passar o CPF para string ??
- [x] Consertar a get_string


## Tarefas - 30/09

- [ ] Aprimoramento do Cadastra Produto
- [ ] Implementação do Mostra Clientes
- [ ] Implementação das Exceções




Sobre a atualização do produto:

Vai achar o produto no arquivo, atualizar o dado, depois regravar no arquivo

a regravação vai acontecer da seguinte forma:

- grava todo mundo num arquivo temporário, exceto o que foi atualizado
- grava o atualizado na última posição

### Esboço do Programa final

A `main` do programa final será um menu divido em 3 ítens:

- Modo Venda
- Modo Estoque
- Modo Recomendação

1.  **Modo Venda**

- Digitar o nome do Cliente
  - Se ele não existir, fazer o cadastro
- Adicionar o ítem
  - Se o produto não existir, lançar um exceção
  - Se o protudo existir, mas não possuir estoque, lançar outra exceção
  - Perguntar ao usuário se ele deseja adicionar um novo ítem
    - se não quiser, encerra a compra mostrando
      - total da venda
      - se for sócio, lançar o desconto e mostra
        - total com desconto e  o desconto

2. **Modo Estoque**

Este modo será formado por 4 submenus

- Adiciona ítem
- Atualiza ítem
- exclui ítem
- mostra todos os ítems (resolver a parte do sort depois)

3. **Modo Recomendação**

Não foi possível a implementação

#### Observação

Descobri que dá pra fazer `malloc()` com objetos. é só fazer um cast pro tipo, kkkkkk!

limpeza do código: deixar a `get_string()` só pra pegar a string mesmo
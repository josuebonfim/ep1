#ifndef FUNCOES_UTEIS_HPP
#define FUNCOES_UTEIS_HPP

#include <iostream>
#include <string>

using namespace std;

string get_string();
string valida_string(string valor);

long int get_long_int();
int get_int();
float get_float();

string li_to_string(long int cpf);

#endif
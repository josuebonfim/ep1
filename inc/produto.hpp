#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <iostream>
#include "objeto.hpp"

using namespace std;

class Produto : public Objeto{
private:
    int quantidade;
    float preco;
    string categoria;
public:
    Produto();
    Produto(string nome, int quantidade, string categoria);
    ~Produto();
    int get_quantidade();
    void set_quantidade(int quantidade);
    float get_preco();
    void set_preco(float preco);
    string get_categoria();
    void set_categoria(string categoria);
    void imprime_dados();
    void cadastra_produto();
};

#endif
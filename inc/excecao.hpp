#ifndef EXCECAO_HPP
#define EXCECAO_HPP

#include <exception>
#include <iostream>

using namespace std;

class Excecao : public exception
{
private: 
    const char *mensagem;
    Excecao();
public:
    Excecao(const char *s) throw (): mensagem(s){};    
    const char *what() const throw();
};

#endif
#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <iostream>
#include "objeto.hpp"

using namespace std;

class Cliente : public Objeto
{
private:
    long int cpf;
    string email;
    string telefone;
    string data_de_nascimento;
    int qtd_de_compras;
    int socio;
public:
    Cliente();
    Cliente(string nome, long int cpf, string email, string telefone, string data_de_nascimento, int qtd_de_vendas, int socio);
    ~Cliente();
    long int get_cpf();
    void set_cpf(long int cpf);
    string get_email();
    void set_email(string email);
    string get_telefone();
    void set_telefone(string telefone);
    string get_data_de_nascimento();
    void set_data_de_nascimento(string data_de_nascimento);
    int get_qtd_de_compras();
    void set_qtd_de_compras(int qtd_de_compras);
    int get_socio();
    void set_socio(int socio);
    void cadastra_cliente();
    void imprime_dados();
};

#endif
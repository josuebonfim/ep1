#ifndef OBJETO_HPP
#define OBJETO_HPP

#include <iostream>

using namespace std;

class Objeto
{
private: 
    string nome;
    string tipo;
public:
    Objeto();
    Objeto(string nome, string tipo);
    ~Objeto();
    string get_nome();
    void set_nome(string nome);
    string get_tipo();
    void set_tipo(string tipo);
};


#endif 
#include "cliente.hpp"
#include "funcoes_uteis.hpp"
#include <iostream>

using namespace std;

#define QTD_COMPRAS 10

Cliente::Cliente(){}

Cliente::Cliente(string nome, long int cpf, string email, string telefone, string data_de_nascimento, int qtd_de_vendas, int socio){
    set_tipo("Cliente");
    set_nome(nome);
    set_cpf(cpf);
    set_email(email);
    set_telefone(telefone);
    set_data_de_nascimento(data_de_nascimento);
    set_qtd_de_compras(qtd_de_compras);
    cout << get_qtd_de_compras() << endl;
    if(get_qtd_de_compras() > QTD_COMPRAS)
        set_socio(1);
    else
        set_socio(0);
}

Cliente::~Cliente(){}

long int Cliente::get_cpf(){
    return cpf;
}

void Cliente::set_cpf(long int cpf){
    this->cpf = cpf;
}

string Cliente::get_email(){
    return email;
}

void Cliente::set_email(string email){
    this->email = email;
}

string Cliente::get_telefone(){
    return telefone;
}

void Cliente::set_telefone(string telefone){
    this->telefone = telefone;
}

string Cliente::get_data_de_nascimento()
{
    return data_de_nascimento;
}

void Cliente::set_data_de_nascimento(string data_de_nascimento){
    this->data_de_nascimento = data_de_nascimento;
}

int Cliente::get_qtd_de_compras()
{
    return qtd_de_compras;
}

void Cliente::set_qtd_de_compras(int qtd_de_compras){
    this->qtd_de_compras = qtd_de_compras;
}

int Cliente::get_socio(){
    return socio;
}

void Cliente::set_socio(int socio){
    this->socio = socio;
}

void Cliente::cadastra_cliente()
{
    string nome_temp;
    long int cpf_temp;
    string email_temp;
    string telefone_temp;
    string data_nas_temp;

    cout << endl;
    cout << "Cadastro de Novo Cliente" << endl;
    cout << "------------------------" << endl;
    cout << "Digite o Nome do cliente: ";
    set_nome(get_string());
    cout << "Digite o CPF: ";
    set_cpf(get_long_int());
    cout << "Digite o e-mail do cliente: ";
    set_email(get_string());
    cout << "Digite o telefone do cliente: ";
    set_telefone(get_string());
    cout << "Digite a data de nascimento do cliente: ";
    set_data_de_nascimento(get_string());
    set_qtd_de_compras(0);
    cout << endl << endl;

    imprime_dados();
}

void Cliente::imprime_dados(){
    cout << "----------------------------" << endl;
    cout << "Nome: " << get_nome() << endl;
    cout << "CPF: " << get_cpf() << endl;
    cout << "e-mail: " << get_email() << endl;
    cout << "Telefone: " << get_telefone() << endl;
    cout << "Data de nascimento: " << get_data_de_nascimento() << endl;
    if(get_qtd_de_compras() > QTD_COMPRAS)
    {
        cout << "É sócio." << endl;
        set_socio(1);
    }
    else
    {
        cout << "Não é socio." << endl;
        set_socio(0);
    }
    
    cout << "-------------------------" << endl << endl;
}